export class Level2 {
    constructor (
        public name?: string,
        public id_type?: string,
        public id_number?: string,
        public id_scan?: string,
    ) {}
}
export class Level3 {
    constructor (
        public address?: string,
        public city?: string,
        public country?: string,
    ) {}
}
export class Level4 {
    constructor (
        public company_name?: string,
        public country_name?: string,
    ) {}
}
