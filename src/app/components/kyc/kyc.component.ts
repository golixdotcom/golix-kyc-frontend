import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Headers } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';

import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Level2 } from '../../models/verification_fields';

interface DocumentType {
  id: string;
  name: string;
}

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['../kyclvl3/kyclvl3.component.css']
})

export class KycComponent implements OnInit {
  detail = new Level2;
  submitted = false;
  document_name: string;
  error: string;
  successmessage = '';
  submittedsuccessfully = true;
  documentTypePlaceholder: string;
  documentTypes: Array<DocumentType> = [{
    id: 'national_id',
    name: 'National ID'
  }, {
    id: 'passport',
    name: 'Passport'
  }];
  filesToUpload: Array<File> = [];
  document: File;
  document_selfie: File;
  docSubmitted = false;
  docSubmitted2 = false;

  formData: any;
  files: Array<File> = this.filesToUpload;

  constructor(
    private http: Http,
    private spinner: NgxSpinnerService
  ) {
  }

  ngOnInit() {
    this.setDocumentTypePlaceholder();
    this.submittedsuccessfully = false;
  }

  setDocumentTypePlaceholder() {
    // tslint:disable-next-line:max-line-length
    this.documentTypePlaceholder = (this.detail.id_type && this.detail.id_type !== '') ? this.documentTypes.filter(dT => dT.id === this.detail.id_type)[0].name : 'ID/Passport';
  }

  documentTypeChanged() {
    this.setDocumentTypePlaceholder();
  }

  documentChangeEvent(fileInput: any) {
    if (fileInput.target.files.length > 0) {
      this.document = <File>fileInput.target.files[0];
      this.docSubmitted = true;
    } else {
      this.docSubmitted = false;
    }
  }

  documentSelfieChangeEvent(fileInput: any) {
    if (fileInput.target.files.length > 0) {
      this.document_selfie = <File>fileInput.target.files[0];
      this.docSubmitted2 = true;
    } else {
      this.docSubmitted2 = false;
    }
  }

  getCookie(cookie: string) {
    return (document.cookie == '') ? null : document.cookie.split(";").map(c => {
      let tmp = c.replace(" ", "").split("="),
        key = tmp[0],
        value = tmp[1];

      let temp = {};
      temp[key] = value;

      return temp;
    }).filter(c => c[cookie] != null)[0][cookie];
  }

  upload(documentForm) {
    this.spinner.show();
    this.submittedsuccessfully = true;
    let token = this.getCookie('auth_token');
    this.formData = new FormData();
    this.formData.append('document', this.document, this.document.name, this.document.type);
    this.formData.append('document_selfie', this.document_selfie, this.document_selfie.name, this.document.type);
    this.formData.append('name', documentForm.name);
    this.formData.append('document_type', documentForm.id);
    this.formData.append('document_number', documentForm.id_number);
    this.http.post(`${environment.kycBaseUrl}/documents/identification`, this.formData, {
      headers: new Headers({
        token
      })
    })
      .pipe(
        catchError(this.handleError('upload'))
      ).subscribe((data: any) => {
        this.spinner.hide();
        if (data) {
          const response = data.json();
          if (response.message === 'DOCUMENTS UPLOADED.') {
            this.successmessage = 'Documents Uploaded Successfully.';
            setTimeout(()=> {
              window.location.href = 'https://golix.com/settings';
            },1500)
          } else {
            this.error = 'Documents not uploaded. Please try again.';
          }
        } else {
          this.error = 'Failed to upload documents. Please try again.';
        }
      });
      // .subscribe(files => console.log('files', files));
  }


  verify(detail, documentForm) {
    this.submitted = true;
    if (
      detail.name != null &&
      detail.id_type != null &&
      detail.id_number != null &&
      detail.id_number !== '' &&
      this.docSubmitted &&
      this.docSubmitted2
    ) {
      this.upload(documentForm);
    }
  }
    /**

  * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): any => {

    // TODO: send the error to remote logging infrastructure
    console.error('err', error);
    this.spinner.hide();
     // log to console instead

    // this is a hack, to be able to get the error object from login, since I don't know how this works. BP
    const response = JSON.parse(error._body);
    if (response.message === 'FAILED TO UPLOAD DOCUMENTS.') {
      this.error = 'Failed to submit documents. Please check size .....';
      this.spinner.hide();
    } else {
      this.error = 'Failed to submit documents. Please try again.';
      this.spinner.hide();
    }
    // this.error = JSON.parse(error._body);
    // TODO: better job of transforming error for user consumption

    // Let the app keep running by returning an empty result.
    // return Observable.of(result as T);
  };
}

}
