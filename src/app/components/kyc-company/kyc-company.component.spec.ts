import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycCompanyComponent } from './kyc-company.component';

describe('KycCompanyComponent', () => {
  let component: KycCompanyComponent;
  let fixture: ComponentFixture<KycCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
