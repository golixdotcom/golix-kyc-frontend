import { Component, OnInit, isDevMode } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Headers } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';

import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Level4 } from '../../models/verification_fields';
import countries from '../../models/countries';


@Component({
  selector: 'app-kyc-company',
  templateUrl: './kyc-company.component.html',
  styleUrls: ['../kyclvl3/kyclvl3.component.css']
})
export class KycCompanyComponent implements OnInit {
  listcountries = countries;
  detail = new Level4;
  submitted = false;
  countrySet = false;
  companyName: string;
  error: string;
  success: string;
  submittedsuccessfully = true;
  document_company: File;
  docSubmitted = false;
  formData: any;
  filesToUpload: Array<File> = [];
  files: Array<File> = this.filesToUpload;
  constructor(
    private http: Http,
    private spinner: NgxSpinnerService
  ) {
  }


  ngOnInit() {
    this.submittedsuccessfully = false;
  }

  documentChangeEvent(fileInput: any) {
    if (fileInput.target.files.length > 0) {
      this.filesToUpload = fileInput.target.files;
      this.docSubmitted = true;
    } else {
      this.docSubmitted = false;
    }
  }

  getCookie(cookie: string) {
    return (document.cookie == '') ? null : document.cookie.split(";").map(c => {
      let tmp = c.replace(" ", "").split("="),
        key = tmp[0],
        value = tmp[1];

      let temp = {};
      temp[key] = value;

      return temp;
    }).filter(c => c[cookie] != null)[0][cookie];
  }

  upload(documentForm) {
    const files: Array <File> = this.filesToUpload;
    this.spinner.show();
    this.submittedsuccessfully = true;
    const token = this.getCookie('auth_token');
    this.formData = new FormData();
   for (let i = files.length - 1; i >= 0 ; i--) {
   this.formData.append('documents', files[i], files[i]['name']);
   }
    this.formData.append('name', documentForm.company);
    this.formData.append('country', documentForm.country_name);
    console.log(documentForm);
    this.http.post(`${environment.kycBaseUrl}/documents/company`, this.formData, {
      headers: new Headers({
        token
      })
    })
      .pipe(
        catchError(this.handleError('upload'))
      ).subscribe((data: any) => {
        this.spinner.hide();
        if (data) {
          const response = data.json();
          if (response.message === 'DOCUMENTS UPLOADED.') {
             window.location.href = 'https://golix.com/settings';
          } else {
            this.error = 'Documents not uploaded. Please try again.';
          }
        } else {
          this.error = 'Failed to upload document. Please try again.';
        }
      });
      // .subscribe(files => console.log('files', files));
  }

  verify(detail, documentForm) {
    this.submitted = true;
    if (
      detail.company_name !== '' &&
      detail.country_name !== '' &&
      detail.country_name != null &&
      detail.country_name !== 'Country' &&
      this.docSubmitted
    ) {
      this.upload(documentForm);
    }
  }


      /**

  * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): any => {

    // TODO: send the error to remote logging infrastructure
    console.error('err', error);
    this.spinner.hide();
    // log to console instead

    // this is a hack, to be able to get the error object from login, since I don't know how this works. BP
    const response = JSON.parse(error._body);
    if (response.message === 'FAILED TO UPLOAD DOCUMENTS.') {
      this.error = 'Failed to submit document. Please check size .....';
      this.spinner.hide();
    } else {
      this.error = 'Failed to submit document. Please try again.';
      this.spinner.hide();
    }
    // this.error = JSON.parse(error._body);
    // TODO: better job of transforming error for user consumption

    // Let the app keep running by returning an empty result.
    // return Observable.of(result as T);
  };
}

}
