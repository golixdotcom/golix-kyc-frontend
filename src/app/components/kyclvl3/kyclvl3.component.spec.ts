import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Kyclvl3Component } from './kyclvl3.component';

describe('Kyclvl3Component', () => {
  let component: Kyclvl3Component;
  let fixture: ComponentFixture<Kyclvl3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kyclvl3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Kyclvl3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
