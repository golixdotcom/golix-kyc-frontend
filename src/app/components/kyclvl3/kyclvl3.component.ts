import { Component, OnInit, isDevMode } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Headers } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';

import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Level3 } from '../../models/verification_fields';
import countries from '../../models/countries';


@Component({
  selector: 'app-kyclvl3',
  templateUrl: './kyclvl3.component.html',
  styleUrls: ['./kyclvl3.component.css']
})
export class Kyclvl3Component implements OnInit {
  listcountries = countries;
  detail = new Level3;
  submitted = false;
  countrySet = false;
  addressNumber: string;
  addressTown: string;
  addressCountry;
  error: string;
  success: string;
  successmessage = '';
  submittedsuccessfully = true;
  document_address: File;
  docSubmitted = false;
  formData: any;
  filesToUpload: Array<File> = [];
  files: Array<File> = this.filesToUpload;
  constructor(
    private http: Http,
    private spinner: NgxSpinnerService
  ) {
  }


  ngOnInit() {
    this.submittedsuccessfully = false;
  }

  documentChangeEvent(fileInput: any) {
    if (fileInput.target.files.length > 0) {
      this.document_address = <File>fileInput.target.files[0];
      this.docSubmitted = true;
    } else {
      this.docSubmitted = false;
    }
  }

  getCookie(cookie: string) {
    return (document.cookie == '') ? null : document.cookie.split(";").map(c => {
      let tmp = c.replace(" ", "").split("="),
        key = tmp[0],
        value = tmp[1];

      let temp = {};
      temp[key] = value;

      return temp;
    }).filter(c => c[cookie] != null)[0][cookie];
  }

  upload(documentForm) {
    this.spinner.show();
    this.submittedsuccessfully = true;
    let token = this.getCookie('auth_token');
    this.formData = new FormData();
    this.formData.append('document', this.document_address);
    this.formData.append('address', documentForm.address);
    this.formData.append('city', documentForm.city);
    this.formData.append('country', documentForm.country);
    this.http.post(`${environment.kycBaseUrl}/documents/residence`, this.formData, {
      headers: new Headers({
        token
      })
    })
      .pipe(
        catchError(this.handleError('upload'))
      ).subscribe((data: any) => {
        if (data) {
          const response = data.json();
          if (response.message === 'DOCUMENTS UPLOADED.') {
            this.successmessage = 'Uploading Documents, please wait';
            this.spinner.hide();
             window.location.href = 'https://golix.com/settings';
          } else if(response.message== "LEVEL TWO VERIFICATION REQUIRED."){
            this.error = 'Please complete your Level Two Verification first.';
            this.spinner.hide();
          }
        } else {
          this.error = 'Failed to upload document. Please try again.';
          this.spinner.hide();
        }
      });
      // .subscribe(files => console.log('files', files));
  }

  verify(detail, documentForm) {
    this.submitted = true;
    if (
      detail.address !== '' &&
      detail.city !== '' &&
      detail.country !== '' && 
      detail.country != null &&
      detail.country !== 'Country' &&
      this.docSubmitted
    ) {
      this.upload(documentForm);
    }
  }


      /**

  * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): any => {

    // TODO: send the error to remote logging infrastructure
    console.error('err', error);
    this.spinner.hide();
    // log to console instead

    // this is a hack, to be able to get the error object from login, since I don't know how this works. BP
    const response = JSON.parse(error._body);
    if (response.message === 'FAILED TO UPLOAD DOCUMENTS.') {
      this.error = 'Failed to submit document. Please check size .....';
      this.spinner.hide();
    } else {
      this.error = 'Failed to submit document. Please try again.';
      this.spinner.hide();
    }
    // this.error = JSON.parse(error._body);
    // TODO: better job of transforming error for user consumption

    // Let the app keep running by returning an empty result.
    // return Observable.of(result as T);
  };
}

}
