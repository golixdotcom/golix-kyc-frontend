import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { KycComponent } from './components/kyc/kyc.component';
import { Kyclvl3Component } from './components/kyclvl3/kyclvl3.component';
import { FooterComponent } from './footer/footer.component';
import { KycCompanyComponent } from './components/kyc-company/kyc-company.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    KycComponent,
    Kyclvl3Component,
    FooterComponent,
    KycCompanyComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/identity', pathMatch: 'full' },
      {
        path: 'identity',
        pathMatch: 'full',
        component: KycComponent
      },
      {
        path: 'residence',
        pathMatch: 'full',
        component: Kyclvl3Component
      },
      {
        path: 'company',
        pathMatch: 'full',
        component: KycCompanyComponent
      }
    ], { useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
